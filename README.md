Wantzel
=======

IRC bot in python, used to help for press review of La Quadrature du Net

License: AGPLv3
Source : http://git.laquadrature.net/rp/wantzel


Installation
~~~~~~~~~~~~

Clone the repository, then create a virtualenv with a python2.7 interpreter.
Install the requirements and launch it using wantzel.py.


Tests
~~~~~

Install the tests requirements in an other virtual environment, create a specific database for
tests, set tests/config.py as needed and launch tests using nosetests.

