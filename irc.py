#encoding: utf-8
"""
A simplistic IRC client connecting to freenode network.
"""

from twisted.internet import reactor, protocol
from twisted.words.protocols import irc

class IrcClient(irc.IRCClient):
    def __init__(self, config):
        self.config = config
        self.nickname = config.nickname
        self.password = config.password

    def connectionMade(self):
        irc.IRCClient.connectionMade(self)

    def connectionLost(self, reason):
        irc.IRCClient.connectionLost(self, reason)

    def signedOn(self):
        for channel in self.config.channels:
            self.join(channel)


class IrcClientFactory(protocol.ClientFactory):
    def __init__(self, config):
        self.config = config

    def set_callbacks(self):
        pass

    def buildProtocol(self, addr):
        print("Building protocol")
        self.client = IrcClient(self.config)
        self.set_callbacks()
        return self.client

    def clientConnectionLost(self, connector, reason):
        """If we get disconnected, reconnect to server."""
        connector.connect()

    def clientConnectionFailed(self, connector, reason):
        print("Connection failed: %s" % reason)
        reactor.stop()
