#encoding: utf-8
"""
Monitoring methods.
"""

import feedparser
import sqlite3
import time

import config
from logs import Log

class Monitor():
    def __init__(self, name, url):
        # Monitor's name
        self.name = name
        # base url
        self.url = url
        # default last_entry_published
        self.last_entry_updated = time.strptime("2000-01-01 00:00:00 CET", "%Y-%m-%d %H:%M:%S %Z")
        # See if there is a later last_entry_published for wiki
        connection = sqlite3.connect(config.sqlite_db)
        for row in connection.execute(
            "SELECT last_entry_updated FROM monitors WHERE name=?",
            (self.name,)
        ):
            self.last_entry_updated = time.strptime(
                row[0].encode("utf-8"),
                "%Y-%m-%d %H:%M:%S %Z"
            )
        # In case there is no entry for this monitor in database
        if self.last_entry_updated==time.strptime("2000-01-01 00:00:00 CET", "%Y-%m-%d %H:%M:%S %Z"):
            last_entry_updated = time.strftime(
                "%Y-%m-%d %H:%M:%S %Z",
                self.last_entry_updated
            )
            connection.execute("INSERT INTO monitors VALUES (?,?)", (last_entry_updated, self.name))
            connection.commit()
        Log.debug("Dernière mise à jour de %s: %s" % (self.name, self.last_entry_updated))

    def set_last_entry_updated(self, last_entry_updated):
        self.last_entry_updated = last_entry_updated
        last_entry_updated = time.strftime(
            "%Y-%m-%d %H:%M:%S %Z",
            self.last_entry_updated
        )
        connection = sqlite3.connect(config.sqlite_db)
        connection.execute(
            "UPDATE monitors SET last_entry_updated=? WHERE name=?",
            (last_entry_updated, self.name)
        )
        connection.commit()

    def update(self):
        pass


class Mediawiki(Monitor):
    def update(self):
        url = self.url + "api.php?days=1&limit=50&translations=filter&action=feedrecentchanges&feedformat=atom"
        now = time.localtime()
        today = time.strptime("%s-%s-%s %s" % (
            now.tm_year,
            now.tm_mon,
            now.tm_mday,
            time.tzname[0]
            ), "%Y-%m-%d %Z")
        entries = feedparser.parse(url)['entries']
        for entry in entries:
            # if date of update is greater than today midnight
            if today < entry.updated_parsed:
                if self.last_entry_updated < entry.updated_parsed:
                    # Save last_entry_published
                    self.set_last_entry_updated(entry.updated_parsed)
                    # Sending monitoring on working chan
                    return """[%s] %s a mis à jour la page %s => %s""" % (
                        self.name,
                        entry.author.encode("utf-8"),
                        entry.title.encode("utf-8"),
                        entry.link.encode("utf-8"),
                    )

class GitlabGroup(Monitor):
    def update(self):
        url = self.url[:-1] + ".atom"
        now = time.localtime()
        today = time.strptime("%s-%s-%s %s" % (
            now.tm_year,
            now.tm_mon,
            now.tm_mday,
            time.tzname[0]
            ), "%Y-%m-%d %Z")
        entries = feedparser.parse(url)['entries']
        for entry in entries:
            # if date of update is greater than today midnight
            if today < entry.updated_parsed:
                if self.last_entry_updated < entry.updated_parsed:
                    # Save last_entry_published
                    self.set_last_entry_updated(entry.updated_parsed)
                    # Sending monitoring on working chan
                    return """[%s] %s => %s""" % (
                        self.name,
                        entry.title.encode("utf-8"),
                        entry.link.encode("utf-8"),
                    )

class Monitoring():
    def __init__(self):
        # List of objects to monitor
        self.monitors = []
        for monitor in config.monitors:
            if monitor["type"]=="mediawiki":
                self.monitors.append(Mediawiki(monitor["name"], monitor["url"]))
            elif monitor["type"]=="gitlab_group":
                self.monitors.append(GitlabGroup(monitor["name"], monitor["url"]))

    def update(self):
        """
        This method loops over each monitor to see if something has changed.
        """
        messages = []
        for monitor in self.monitors:
            message = monitor.update()
            if message is not None:
                messages.append(message)
        return messages
