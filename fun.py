#encoding: utf-8
"""
Fun commands.
"""

from random import choice

def fun(user, channel, message):
    """
    This function is there to add some fun commands which could be not used easily.
    """
    # Specific answer to Deltree, the animal's joke
    if user.lower()=="deltree" and msg=="\_o< ~ Coin ~ >o_/":
        animal = choice([
            {
                "left": """><((('>""",
                "right": """<')))><""",
                "sound": "blub",
            },
            {
                "left": """=^..^=""",
                "right": """=^..^=""",
                "sound": "meow",
            },
            {
                "left": """ˁ˚ᴥ˚ˀ""",
                "right": """ˁ˚ᴥ˚ˀ""",
                "sound": "wouf",
            },
            {
                "left": """\_o<""",
                "right": """>o_/""",
                "sound": "coin",
            },
            {
                "left": """^(*(oo)*)^""",
                "right": """^(*(oo)*)^""",
                "sound": "grouïk",
            },
            {
                "left": """~~(__^·>""",
                "right": """<·^__)~~""",
                "sound": "yiik",
            },
        ])
        return "%s ~ %s ~ Deltree ~ %s ~ %s" % (
            animal["left"],
            animal["sound"],
            animal["sound"],
            animal["right"],
        )
    return ""
